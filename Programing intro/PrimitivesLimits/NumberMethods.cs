﻿namespace PrimitivesLimits
{
    public class NumberMethods
    {
        public static bool IsOdd(int num)
        {
            return num % 2 == 1 || num %2 ==-1;
        }

        public static bool IsPrime(int num)
        {
            if(num < 0)
            {
                return false;
            }
            if(num %2 == 0)
            {
                return num == 2;
            }
            else
            {
                int limit = num / 2;

                for(int i =3; i< limit; i += 2)
                {
                    if(num % i == 0)
                    {
                        return false;
                    }
                }

                return num !=1;
            }
        }

        public static int MinElement(int[] arr)
        {
            if(arr.Length == 0)
            {
                return -1;
            }
            Array.Sort(arr);
            return arr[0];
        }

        public static int KthMinElement(int k, int[] arr)
        {
            if (arr.Length == 0 || k < 0 || k >= arr.Length)
            {
                return -1;
            }
            Array.Sort(arr);
            return arr[k];
        }

        public static int GetOddOccurences(int[] array)
        {
            for(int i = 0; i < array.Length; i++)
            {
                int elem = array[i];
                int occurences = 0;
                for(int j = 0; j<array.Length; j++)
                {
                    if (array[j] == elem)
                    {
                        occurences++;
                    }
                }

                if(occurences %2 == 1)
                {
                    return elem;
                }
            }
            return -1;
        }

        public static int GetAverage(int[] array)
        {
            int avg = 0;
            if (array.Length > 0)
            {
                for (int i = 0; i < array.Length; i++)
                {
                    avg += array[i];
                }

                avg /= array.Length;
            }
            return avg;
        }

        public static long Pow(int a, int b)
        {
            if(b == 0)
            {
                return 1;
            }else if (b == 1)
            {
                return a;
            }else if(b %2 == 1)
            {
                return a * Pow(a, b - 1);
            }
            else
            {
                return Pow(a * a, b / 2);
            }
        }

        static long Factoriel(int i)
        {
            if(i <= 2)
            {
                return i;
            }
            return i * Factoriel(i - 1);
        }

        public static long DoubleFactoriel(int i)
        {
            int fac1 = (int)Factoriel(i);

            return Factoriel(fac1);
        }

        public static long KthFactoriel(int k, int i)
        {
            if(k == 1)
            {
                return Factoriel(i);
            }

            int fac1 = (int)Factoriel(i);
            return KthFactoriel(k - 1, fac1);
        }

        static int Gcd(int a,int b)
        {
            if(a < b)
            {
                int swap = a;
                a = b;
                b = swap;
            }
            if(a % b == 0)
            {
                return b;
            }
            else
            {
                return Gcd(b, a % b);
            }
        }

        static long Lcn(int a, int b)
        {
            return (a * b) / Gcd(a, b);
        }

        public static long LowestDivisibleNumber(int n)
        {
            if(n == 0)
            {
                return 0;
            }

            long lcn = 1;
            
            for(int i = 2; i<=n; i++)
            {
                lcn = Lcn((int)lcn, i);
            }

            return lcn;
        }
    }
}
